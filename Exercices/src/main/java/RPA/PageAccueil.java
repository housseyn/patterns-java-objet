package RPA;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class PageAccueil {

    @FindBy(xpath = "/html/body/app-root/div[2]/app-rpa1/div/div[1]/div[6]/button")
    private WebElement startButton;

    @FindBy(xpath = "//input[@ng-reflect-name='labelFirstName']")
    private WebElement firstNameInput;

    @FindBy(xpath = "//input[@ng-reflect-name='labelLastName']")
    private WebElement lastNameInput;

    @FindBy(xpath = "//input[@ng-reflect-name='labelCompanyName']")
    private WebElement companyNameInput;

    @FindBy(xpath = "//input[@ng-reflect-name='labelAddress']")
    private WebElement addressInput;

    @FindBy(xpath = "//input[@ng-reflect-name='labelRole']")
    private WebElement roleInput;

    @FindBy(xpath = "//input[@ng-reflect-name='labelEmail']")
    private WebElement emailInput;

    @FindBy(xpath = "//input[@ng-reflect-name='labelPhone']")
    private WebElement phoneInput;

    @FindBy(xpath = "//input[@value='Submit']")
    private WebElement submitButton;

    private ArrayList<Map<String, String>> listJdd;

    private static final String pathCSV = "src/test/resources/english.csv";

    public PageAccueil() {
        this.listJdd = loadCsvJDD();
    }

    private ArrayList<Map<String, String>> loadCsvJDD() {
        try {
            List<String> lines = Files.readAllLines(Paths.get(pathCSV));
            String[] headers = lines.get(0).split(",");
            return lines.stream().skip(1).map(line -> {
                String[] values = line.split(",");
                Map<String, String> row = new HashMap<>();
                for (int i = 0; i < headers.length; i++) {
                    row.put(headers[i], values[i]);
                }
                return row;
            }).collect(Collectors.toCollection(ArrayList::new));
        } catch (IOException e) {
            throw new RuntimeException("Error reading CSV file", e);
        }
    }

    public void startChallenge() {
        clickMethod(startButton);
        for (int i = 0; i < 10; i++) {
            Map<String, String> data = listJdd.get(i);
            firstNameInput.sendKeys(data.get("First Name"));
            lastNameInput.sendKeys(data.get("Last Name"));
            companyNameInput.sendKeys(data.get("Company Name"));
            addressInput.sendKeys(data.get("Address"));
            roleInput.sendKeys(data.get("Role in Company"));
            emailInput.sendKeys(data.get("Email"));
            phoneInput.sendKeys(data.get("Phone Number"));
            submitButton.click();
        }
    }

    private void clickMethod(WebElement element) {
        element.click();
    }
}
