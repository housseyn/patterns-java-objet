package Petstore;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class PageHeritage {

    // WebElement représentant le champ de recherche sur la page
    @FindBy(xpath="//input[@name='keyword']")
    WebElement field_search;

    // WebElement représentant le bouton de recherche sur la page
    @FindBy(xpath="//input[@name='searchProducts']")
    WebElement bouton_search;

    public void clicSearch(String recherche) {
        // Saisit le terme de recherche dans le champ de recherche
        field_search.sendKeys(recherche);
        // Clique sur le bouton de recherche
        bouton_search.click();
    }
}
