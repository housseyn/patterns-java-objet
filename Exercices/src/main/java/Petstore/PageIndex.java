package Petstore;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import static org.junit.Assert.*;

public class PageIndex extends PageHeritage {

    // WebElement représentant le bouton "Sign In" sur la page d'index
    @FindBy(xpath = "//*[@id=\"MenuContent\"]/a[2]")
    private WebElement bouton_signin;

    // WebElement représentant le bouton "Fish" sur la page d'index
    @FindBy(xpath = "//*[@id=\"QuickLinks\"]/a[1]/img")
    private WebElement bouton_fish;

    // WebElement représentant l'élément contenant le texte 'Tiger Shark' sur la page d'index
    @FindBy(xpath = "//*[contains(text(), 'Tiger Shark')]")
    private WebElement assertFish;

    // Clique sur le bouton "Sign In" et retourne la page de connexion
    public PageLogin clicSignIn(WebDriver driver) {
        bouton_signin.click();
        return PageFactory.initElements(driver, PageLogin.class);
    }

    // Clique sur le bouton "Fish", vérifie l'affichage de l'élément "assertFish", et retourne la page Fish
    public PageFish clicFish(WebDriver driver) {
        bouton_fish.click();
        assertTrue("L'élément 'Tiger Shark' n'est pas affiché comme attendu.", assertFish.isDisplayed());
        return PageFactory.initElements(driver, PageFish.class);
    }
}
