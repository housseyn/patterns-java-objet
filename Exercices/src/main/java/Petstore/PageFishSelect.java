package Petstore;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class PageFishSelect extends PageHeritage {

    // WebElement représentant le bouton "Add to Cart" sur la page de sélection des poissons
    @FindBy(xpath="//*[@id=\"Catalog\"]/table/tbody/tr[2]/td[5]/a")
    WebElement bouton_AddToCart;

    // WebElement représentant le titre du panier sur la page de sélection des poissons
    @FindBy(xpath="//*[@id=\"Cart\"]/h2")
    WebElement title_cart;

    // Méthode pour cliquer sur le bouton "Add to Cart" et effectuer des vérifications associées
    public void clicAddToCart(WebDriver driver) {
        // Clique sur le bouton "Add to Cart"
        bouton_AddToCart.click();

        // Vérifie que le titre du panier est égal à "Shopping Cart"
        assertEquals("Shopping Cart", title_cart.getText());

        // Vérifie que le titre du panier est affiché sur la page
        assertTrue(title_cart.isDisplayed());
    }
}