package Petstore;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import static org.junit.Assert.assertEquals;

public class PageLogin extends PageHeritage {

    // WebElement représentant le champ de saisie du nom d'utilisateur sur la page de connexion
    @FindBy(xpath="//input[@name='username']")
    WebElement field_username;

    // WebElement représentant le champ de saisie du mot de passe sur la page de connexion
    @FindBy(xpath="//input[@name='password']")
    WebElement field_password;

    // WebElement représentant le bouton de connexion sur la page de connexion
    @FindBy(xpath="//input[@name='signon']")
    WebElement bouton_signon;

    // WebElement représentant le message de bienvenue sur la page d'index après la connexion
    @FindBy(xpath = "//*[@id='WelcomeContent']")
    WebElement msgActuel;

    // Méthode pour cliquer sur le bouton de connexion, saisir les informations d'identification et vérifier le message de bienvenue
    public PageIndex clicSignon(WebDriver driver, String username, String password) {
        // Saisit le nom d'utilisateur
        field_username.sendKeys(username);

        // Efface le champ de mot de passe et saisit le mot de passe
        field_password.clear();
        field_password.sendKeys(password);

        // Clique sur le bouton de connexion
        bouton_signon.click();

        // Récupère le contenu du message de bienvenue
        String contenuBienvenue = msgActuel.getText();

        // Vérifie que le message de bienvenue est "Welcome ABC!"
        assertEquals("Welcome ABC!", contenuBienvenue);

        // Retourne une nouvelle instance de la classe PageIndex après la connexion
        return PageFactory.initElements(driver, PageIndex.class);
    }
}