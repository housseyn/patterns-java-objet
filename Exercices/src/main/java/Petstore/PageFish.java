package Petstore;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class PageFish extends PageHeritage {

    // WebElement représentant le bouton Tiger sur la page Fish
    @FindBy(xpath="//a[@href='/actions/Catalog.action?viewProduct=&productId=FI-SW-02']")
    WebElement bouton_Tiger;

    public PageFishSelect clicFishSelect(WebDriver driver) {
        // Clique sur le bouton Tiger
        bouton_Tiger.click();
        // Retourne une nouvelle instance de la classe PageFishSelect
        return PageFactory.initElements(driver, PageFishSelect.class);
    }
}
