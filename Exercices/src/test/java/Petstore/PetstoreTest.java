package Petstore;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;

public class PetstoreTest {

    // Instance du navigateur WebDriver
    private WebDriver driver;

    // URL de l'application Petstore
    private static final String URL = "https://petstore.octoperf.com/actions/Catalog.action";

    // Chemin du chromedriver sur le système local
    private static final String CHROME_DRIVER_PATH = "src/test/resources/drivers/chromedriver.exe";

    // Méthode exécutée avant chaque cas de test
    @Before
    public void setUp() {
        // Définir le chemin du chromedriver
        System.setProperty("webdriver.chrome.driver", CHROME_DRIVER_PATH);

        // Initialiser une nouvelle instance de ChromeDriver
        driver = new ChromeDriver();

        // Naviguer vers l'URL de l'application Petstore
        driver.get(URL);

        // Maximiser la fenêtre du navigateur
        driver.manage().window().maximize();
    }

    // Cas de test principal
    @Test
    public void executeTest() {
        // Création des instances de pages à utiliser dans le test

        // PageIndex représente la page principale de l'application
        PageIndex pageIndex = PageFactory.initElements(driver, PageIndex.class);

        // PageLogin représente la page de connexion
        PageLogin pageLogin = pageIndex.clicSignIn(driver);

        // PageIndex2 est une instance supplémentaire de la page principale (par exemple, après la connexion)
        PageIndex pageIndex2 = pageLogin.clicSignon(driver, "j2ee", "j2ee");

        // PageFish représente la section Fish de l'application
        PageFish pageFish = pageIndex2.clicFish(driver);

        // PageFishSelect est la page de sélection des poissons
        PageFishSelect pageFishSelect = pageFish.clicFishSelect(driver);

        // Exécution de l'action de recherche de poissons avec le terme "Persian"
        pageFishSelect.clicSearch("Persian");

    }

    // Méthode exécutée après chaque cas de test
    @After
    public void tearDown() {
        // Fermer le navigateur après l'exécution du test
        driver.quit();
    }
}