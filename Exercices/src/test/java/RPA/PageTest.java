package RPA;

import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.PageFactory;

// Classe de test pour la page RPA
public class PageTest {

    // Instance du WebDriver et URL du site à tester
    private WebDriver driver;
    private String URL = "https://www.rpachallenge.com/";

    // Méthode exécutée avant chaque cas de test
    @Before
    public void setup() {
        // Définir le navigateur ChromeDriver
        System.setProperty("webdriver.gecko.driver", "src/test/resources/drivers/geckodriver.exe");

        // Initialiser une nouvelle instance de ChromeDriver
        driver = new FirefoxDriver();

        // Naviguer vers l'URL
        driver.get(URL);

        // Maximiser la fenêtre du navigateur
        driver.manage().window().maximize();
    }

    // Cas de test principal
    @Test
    public void executeTest() throws InterruptedException {

        // Initialiser la page d'accueil en utilisant la méthode PageFactory
        PageAccueil page_accueil = PageFactory.initElements(driver, PageAccueil.class);

        // Exécuter le défi RPA en appelant la méthode startChallenge()
        page_accueil.startChallenge();
    }
}
